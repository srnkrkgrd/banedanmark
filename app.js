var map, newMarker, markerLocation;
$(function(){
  
  // circle size
  var radius = 125;

  // using slider
  $('#slider').slider({
    formater: function(value) {
      radius = value;
      return 'Radius: ' + value + 'm';
    }
  });

  // updating slider radius indication
  $("#slider").on('slide', function(slideEvt) {
    $("#sliderVal").text(slideEvt.value);
  });

  // found addresses
  var addresses = [];
  var uniqueAddresses = [];
  

  // onclicking cpr broker button
  $("#cprbroker").click(function() {
        
        $.getJSON( "personer.json", function(data) {
          
          var mustacheAddresses = {
            'adresser': uniqueAddresses,
            'name': function() {
              var i = parseInt(Math.random() * data.length);

              return data[i].first_name + " " +
                      data[i].last_name;
            },
            'cpr': function() {

              var i = parseInt(Math.random() * data.length);            
              var birthday = data[i].birthday.replace(/\./g,"");
              return birthday.slice(0,4) + birthday.slice(6,8) + "-" +
                      data[i].security;
            }
          };

          var template = "{{#adresser}}<tr><td>{{cpr}}</td><td>{{name}}</td><td>{{.}}</td></tr>{{/adresser}}";
          var html = Mustache.to_html(template, mustacheAddresses);

          $("#cpradresser").html(html);


        });


  });

	// Initialize the map
	var map = L.map('map').setView([55.672720354451386,12.564969062805176], 15);
	L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    	attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
    	maxZoom: 18
	}).addTo(map);
	
  newMarkerGroup = new L.LayerGroup();

  //Extend the default class to use red marker icon
  var RedIcon = L.Icon.Default.extend({
    options: {
      iconUrl: 'marker-icon-red.png' 
    }
  });
  
  var redIcon = new RedIcon();

  map.on('click', function(e) {
    var newMarker = new L.marker(e.latlng).addTo(map);
    L.circle(e.latlng, radius).addTo(map);    
    var lat = newMarker.getLatLng().lat;
    var lng = newMarker.getLatLng().lng;
    
    var url = "http://dawa.aws.dk/adresser/?cirkel="+lng+","+lat+","+radius;
    toastr.info("Forspørger om adresser på (" + lng + "," + lat + "," + radius + ") (lng, lat, radius)");

    $.ajax({
      type: 'GET',
      url: url,
      async: false,
      contentType: "application/json",
      dataType: 'jsonp',
      
      success: function(data) {

        toastr.info("Fandt " + data.length + " adresser.");

        //Add addresses to the map
        for(i=0;i<data.length;i++) {
          addresses.push(data[i].adressebetegnelse);
          var latlng = L.latLng(data[i].adgangsadresse.adgangspunkt.koordinater[1], data[i].adgangsadresse.adgangspunkt.koordinater[0]);

          var newMarker = new L.marker(latlng, {icon: redIcon}).addTo(map);            
        }
        
        //Find the unique addresses
        $.each(addresses, function(i, el){
            if($.inArray(el, uniqueAddresses) === -1) uniqueAddresses.push(el);
        });
        
        toastr.success("Har " + uniqueAddresses.length + " unikke adresser.");

        $("#fundet").text(uniqueAddresses.length);
        
        var mustacheAddresses = {'adresser': uniqueAddresses};
        var template = "{{#adresser}}<tr><td>{{.}}</td></tr>{{/adresser}}";
        var html = Mustache.to_html(template, mustacheAddresses);

        $("#adresser").html(html);
      }
    });
  });
});